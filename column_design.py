"""
Program used to calculate the breakthrough time of H2Se/H2S through an activated carbon bed
Based on approach in: www.adsorption.com/publications/adsorberdes2.pdf
Data on ASTM Standard D6646-03
"""
from numba import jit
def sensitivity():
    import numpy as np
    import matplotlib.pyplot as plt

    Tdr = np.linspace(273.15,1073.15, 100)
    BT_Td = np.ones(len(Tdr))
    lengthr = np.linspace(0.5, 5, 15)
    BT_len = np.ones(len(lengthr))
    radiusr = np.linspace(0.0254,3*.0254, 10)
    BT_rad = np.ones(len(radiusr))
    yr = np.linspace(0.0035,0.1,100)
    F_sccmr = np.linspace(500, 3000, 100)
    Kr = np.linspace(0.1*1.627e5/100**3, 10*1.627e5/100**3, 100)
    BT_K = np.ones(len(Kr))
    BT_2way = np.ones((len(lengthr),len(radiusr)))
    P_drop = np.ones((len(lengthr),len(radiusr)))
# T_sensitivity
#    for i in range(len(Tdr)):
#        BT_Td[i] = curve_eval(Td = Tdr[i])
#
#    plt.plot(Tdr-273.15, BT_Td)
#    plt.xlabel("T (C)")
#    plt.ylabel("Breakthrough Time (operating weeks)")
#    plt.show()

    # length sensitivity
    for i in range(len(lengthr)):
        for j in range(len(radiusr)):
            BT_2way[i,j]= curve_eval(length = lengthr[i], radius=radiusr[j])
            print(lengthr[i], radiusr[j], BT_2way[i,j], P_drop[i,j])

    plt.figure()
    CS = plt.contour(radiusr*100,lengthr,BT_2way,15)
    plt.clabel(CS)
    plt.ylabel("Column Length (m)")
    plt.xlabel("Column Radius (cm)")
    plt.title("50 ppb Breakthrough Time (operating weeks)")
    plt.savefig("Break_contour.png")
    #plt.show()
#    plt.figure()
#    CS = plt.contour(radiusr*100,lengthr,P_drop,15)
#    plt.clabel(CS)
#    plt.ylabel("Column Length (m)")
#    plt.xlabel("Column Radius (cm)")
#    plt.title("Pressure Drop (kPa)")
#    plt.savefig("P_contour.png")

#    plt.plot(lengthr, BT_len)
#    plt.xlabel("Column length (m)")
#    plt.ylabel("Breakthrough Time (operating weeks)")
#    plt.show()

#    # Radius sensitivity
#    for i in range(len(radiusr)):
#        BT_rad[i] = curve_eval(radius = radiusr[i])
#
#    plt.plot(radiusr/0.0254, BT_rad)
#    plt.xlabel("Column radius (inches)")
#    plt.ylabel("Breakthrough Time (operating weeks)")
#    plt.show()

#    # K sensitivity
#    for i in range(len(Kr)):
#        BT_K[i] = curve_eval(K=Kr[i])
#
#    plt.plot(Kr,BT_K)
#    plt.xlabel("Equilibrium Constant")
#    plt.ylabel("Breakthrough Time (operating weeks)")
#    plt.show()
# @jit
def curve_eval(Td = 873.15, length = 1.0, radius = 0.0254, rho_solid = 1500.0,
               F_sccm = 1290.0, y =0.0035,Break_cap = 0.14*100**3/1000,
               K = 10.0**6, D_particle = 0.004, Kinetics = True, limit = 0.05,
               show_curve = False, save_curve=False, show_text = False):
    """Evaluate the breakthrough curve as a function of operating T, column length,
    column diameter, density of adsorbant, flow rate, concentration of H2Se,
    breakthrough capacity, and equilibrium constant.  Choose form of adsorption kinetics:
    True = Yoshida, False = Ranz-Marshall
    Returns time of "limit"  breakthrough (in ppm, default is 0.05 [50 ppb])
    in operating weeks (40 hours)
    """

    import numpy as np
    import matplotlib.pyplot as plt
    import scipy.optimize as opt

    # Constants
    R = 8.314 # J/mol-K
    Ts = 273.15 # K standard temperature
    #Td = 873.15 # K design temperature
    P = 101325 # Pa nominal pressure
    MW_Ar = .039948 # kg/mol
    MW_H2Se = .08098 # kg/mol
    MW_H2S = .0340809 # kg/mol
    D_H2Se_Ar = 10**-3*Td**1.75*((1000*MW_Ar+1000*MW_H2Se)/(MW_Ar*MW_H2Se*1000*1000))**0.5/(
                (16.1**(1/3)+60**(1/3))**2) # Perry's p. 3-285, at 1 atm, 60 is estimated value
    D_H2Se_Ar = D_H2Se_Ar/100**2 # convert diffusivity to m^2/s
    if show_text == True:
        print("D_H2Se (m^2/s) = ", D_H2Se_Ar)

    # Tube dimensions -- Design variables
    # assume 2 inch ID tube
    #length = 1 # m
    #radius = 0.0254 # m
    Ac = np.pi*(radius)**2 # m^2

    # Adsorbant properties
    #rho_solid=1500 # kg/m^3
    # D_particle = .004 # m
    epsilon = .5 # total void fraction (count pores and interstitial space)
    epsilon_bed = .4 # Bed void fraction (don't count pores)
    epsilon_particle = .3 # single particle void fraction (counting pores)
    rho_bed = (1-epsilon)*rho_solid
    rho_particle = rho_bed/(1-epsilon_bed)
    ai = 6*(1-epsilon_bed)/D_particle # interfacial area/volume (1/m)
    psi = 1. # Shape factor (1.0 for beads, 0.91 for pellets, 0.86 for flakes)
    if show_text==True:
        print("ai=", ai)

    # Flow Conditions
    # nominal molar flow rate (mol/s)
    #F_sccm=1290
    F = F_sccm*P/(100**3*R*Ts*60)
    # Nominal vol flow rate (m^3/s)
    Q = F*R*Td/P
    # Fluid density (assume fluid is Argon)
    rho_gas = P*MW_Ar/(R*Td)
    # Fluid viscosity (assume Argon, 600C, Kestin et al., J Chem Phys,  1972 )
    mu_gas = 0.00005
    # superficial velocity (m/s)
    vs = Q/Ac
    # interstitial velocity
    v = vs/epsilon_bed
    if show_text == True:
        print("v (m/s) = ", v)
    # Concentation of H2Se in feed (y is mol frac, Cf is mol/m^3)
    #y = 0.0035
    Cf = y*P/(R*Td)
    if show_text == True:
        print("Cf (mol/m^3) = ", Cf)
    # Reynolds and modified Reynolds Numbers
    Re = rho_gas*vs*D_particle/mu_gas
    Rem = rho_gas*vs/(mu_gas*psi*ai)
    if show_text == True:
        print("Re = ", Re)
        print("Re' = ", Rem)
    # j-factor
    if Re <50:
        jd = 0.91*psi*Rem**(-0.51)
    elif Re<2500:
        jd = 1.17*Re**(-.415)
#    else:
#        return "Reynolds out of Range"

    # Schmidt number
    Sc = mu_gas/(rho_gas*D_H2Se_Ar)
    if show_text == True:
        print("Sc = ", Sc)
    if Kinetics == True:
        # Mass transfer coefficient (Yoshida 1962 correlation) (definition of j-factor)
        k = jd*vs/(Sc**0.667)
    elif Kinetics == False:
        # Mass transfer coefficient (Ranz-Marshall equation 1992)
        Sh = 2.0 + 0.6*Sc**(0.3333)*Re**(0.5)
        k = Sh*D_H2Se_Ar/(D_particle)

    if show_text == True:
        print("k (m/s)= ", k)


    # Ergun Equation for Pressure drop
    P_drop = 150*mu_gas*(1-epsilon_bed)**2*vs*length/(epsilon_bed**3*D_particle**2)+(
             1.75*(1-epsilon_bed)*rho_gas*vs**2*length/(epsilon_bed**3*D_particle))
    if show_text == True:
        print("Pressure drop (Pa) = ", P_drop)

    # Assume, for now, Langmuir isotherm
    # n = A*C_H2Se/(1+B*C_H2Se)
    # dn/dC_H2Se = A/(B*C_H2Se+1)^2
    # n = adsorbant loading; A, B adjustable parameters
    ## Lit params for CO2 adsorption
    ######A = 251.0*1000/100**3 # Langmuir Param m^3/kg (concentration form)
    ######B = 1.627e5/100**3 # Langmuir param m^3/mol (concentration form)
    #Break_cap = 0.14*100**3/1000 # kg H2S/m^3 carbon from Calgon data sheet-use as max loading
    Lambda_max = Break_cap/rho_bed/MW_H2S # mol H2S/kg Carbon Use H2S because of supplier data
    #K = 1.627e5/100**3 # has major effect. not well known--made arbitrarily large
                        # to assume not equilibrium limited.
    A = Lambda_max*K
    B = K
    # Using the Thomas Model (JACS, vol 66, p 1664, 1944)
    nstar = A*Cf/(1+B*Cf)
    rstar = 1/(1+B*Cf)
    if show_text == True:
        print("rstar = ", rstar)
    # Find n_bar/nstar(Cf) -- called CONSTANT
    if rstar < 1:
        CONSTANT = 0.5
    else:
        CONSTANT = 1/(rstar+1)

    Omega = 1+CONSTANT*(rstar-1)
    Delta = k*ai*Cf/(rho_bed*nstar*Omega)
    zeta = Delta*rho_bed*length*nstar/(epsilon*v*Cf)
    def tau(t): return Delta*(t-length/v)
    if show_text == True:
        print("Omega = ", Omega)
        print("Delta = ", Delta)
        print("zeta = ", zeta)
        print("tau(0) = ", tau(0))
        print("zeta*tau(0) = ", zeta*tau(0))


    t = np.linspace(1,500*60*60*8*5,1e6) # 104 weeks max
    Co = np.ones([1e6])
    Total_Fed = np.ones(len(Co))
    Total_Fed = t*F*y
    Breakthrough_time = 0
    p = False
    for i in range(len(Co)):
        Co_n = 1/(1+np.exp((rstar-1)*(tau(t[i])-zeta))) # normalized output concentration
        Co[i] = Co_n*Cf
        #Total_Fed[i]=t[i]*F*y This is just linear--set outside loop
        if Co[i] > limit*P/(10**6*R*Td) and p == False:
            Breakthrough_time=t[i]/60/60/8/5
            p = True
            if not show_curve or not save_curve:
                break
    if Breakthrough_time == 0:
        Breakthrough_time = 500 # Call this max breakthrough time

    if show_curve or save_curve:
        plt.plot(t/60/60/40,Co)
        plt.xlabel("Time (minutes)")
        plt.ylabel("Co (mole/m^3)")
        plt.xlim([0, 60])
    if show_curve:
        plt.show()

    if save_curve:
        plt.savefig('Break_through_curve.png')

    return Breakthrough_time
def Ex1():
    """
    Predict the uptake breakthrough pattern for CO2 at a feed mole fraction
    of y_a = 0.1316 in air by activated carbon
    Predict the inert-purge regeneration of the column.  That is, desorption
    or CO2 using air under the reverse conditions
    """
    import numpy as np
    import matplotlib.pyplot as plt

    y_a_f = 0.1316
    rho_b = 0.36 #g/cc
    d = 0.718 # cm
    P = 1 # atm
    epsilon = 0.345
    L = 73 # cm
    T = 273.15 # K
    d_p = 0.5 # mm
    D_ab = 0.134 # cm^2/s
    Q = 1.85 # sccs
    Psi = 0.8345
    mu = 0.00017 # g/cm-s
    A = 251.0 # Langmuir Param (concentration form)
    B = 1.627e5 # Langmuir param (concentration form)
    Ap = 0.0112 # Langmuir Param (pressure form)
    Bp = 7.258 # Langmuir Param (pressure form)
    R = 82.057 # cm^3-atm/mol-K
    V_bed = np.pi*(d/2)**2*L # cm^3
    Theta = V_bed/Q


    # Uptake Breakthrough
    # Uptake time:
    dndCa = R*T*Ap/(1+Bp*y_a_f)
    print("dn*/dCa = ", dndCa)
    v = Q/(epsilon*np.pi*(d/2)**2)
    print("v = ", v)

    dt_sh = (1+rho_b/epsilon *dndCa)*L/v/60;
    print("Delta_t_SH = ", dt_sh)

    nstar = lambda C: A/(1+B*C)
    jd = 1.277 # Colburn j-factor
    k = 4.86 # cm/s mass tranfer coef
    rstar = 0.5115
    Omega = 0.7557
    Delta = lambda tor: 10.94/tor
    zeta = lambda tor: 8079/tor
    def tau(t,tor): return 10.94*(t-5.514)/tor

    tortuosity = np.linspace(10,1000,5)
    t = np.linspace(1,2000,100)
    Cf = np.ones([5,100])
    for i in range (1, 5):
        for j in range (1, 100):
            Cf[i,j] = 1/(
                    1+np.exp((rstar-1)*(tau(t[j],tortuosity[i])-zeta(tortuosity[i]))))

    plt.plot(t,Cf[0], t,Cf[1],t,Cf[2],t,Cf[3],t,Cf[4])
    plt.show()

def break_time_H2S(C,F,BC,V):
    """
    calculate breakthrough time of H2S in minutes
    C = concentration of H2S, in vol fraction
    F = H2S/air flow rate, in cc/min
    BC = Breakthrough capacity, in gH2S/cc granular activated carbon,
         determined in accordance with ASTM Standard D66460-03
    V = volume of the carbon bed in the absorption tube, in cc
    """
    T = V*BC*1000*22.4/(C*F*34.1)

    return T

def break_time_H2Se(C,F,BC,V):
    """
    calculate breakthrough time of H2Se in minutes
    C = concentration of H2S, in vol fraction
    F = H2S/air flow rate, in cc/min
    BC = Breakthrough capacity, in gH2Se/cc granular activated carbon,
         determined in accordance with ASTM Standard D66460-03
    V = volume of the carbon bed in the absorption tube, in cc
    """
    T = V*BC*1000*22.4/(C*F*80.98)

    return T

def All_H2S():
    """
    calculate the required column size for an All-H2S system
    """
    import numpy as np
    F = 1290 # Flow rate in L/min
    C = 0.0035 # Vol fraction H2S
    BC = 0.14 # g/cc
    h = 90 # tube height in cm
    r = 2*2.54 # tube radius in cm
    V = np.pi*r**2*h
    T = break_time_H2S(C,F,BC,V) # breakthrough time in minutes
    T_weeks = T/60/8/5 # 1 week=8 hours/day, 5 days/week

    print("All H2S: %0.2f weeks; %.2f minutes"%(T_weeks, T))

def All_H2Se():
    """
    calculate the required column size for an All-H2Se system
    """
    import numpy as np
    F = 1290 # Flow rate in L/min
    C = 0.0035 # Vol fraction H2S
    BC = 0.14*80.98/34.1 # g/cc assume that BC for H2Se is proportional to MW
    h = 90 # tube height in cm
    r = 2*2.54 # tube radius in cm
    V = np.pi*r**2*h
    T = break_time_H2Se(C,F,BC,V) # breakthrough time in minutes
    T_weeks = T/60/8/5 # 1 week=8 hours/day, 5 days/week

    print("All H2Se: %0.2f"%(T_weeks))

if __name__ == "__main__":
    #All_H2S()
    #All_H2Se()
    #Ex1()
    #curve_eval()
    sensitivity()
